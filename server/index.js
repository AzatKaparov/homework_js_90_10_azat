const express = require('express');
const cors = require('cors');
const { nanoid } = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};


app.ws('/draw', function (ws, req) {
    const id = nanoid();
    console.log('Client connected! id=', id);
    activeConnections[id] = ws;

    ws.on('message', (data) => {
        const decodedPixels = JSON.parse(data);
        switch (decodedPixels.type) {
            case "CREATE_DRAW": {
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    conn.send(JSON.stringify({
                        type: 'NEW_DRAW',
                        pixelsArray: decodedPixels.pixelsArray
                    }));
                });
                break;
            }
            default:
                console.log('Unknown message type:', decodedPixels.type);
        }
    });

    ws.on('close', (msg) => {
        console.log('Client disconnected! id=', id);
        delete activeConnections[id];
    });
});



app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});