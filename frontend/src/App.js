import React, {useEffect, useRef, useState} from 'react';

const App = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    const canvas = useRef(null);
    const ws = useRef(null);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;
            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.putImageData(imageData, event.clientX, event.clientY);
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        ws.current.send(JSON.stringify({
            type: "CREATE_DRAW",
            pixelsArray: state.pixelsArray
        }));

        setState({...state, mouseDown: false, pixelsArray: []});
    };


    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/draw');

        ws.current.onclose = () => console.log("ws closed");

        ws.current.onmessage = event => {
            const decodedPixels = JSON.parse(event.data);
            if (decodedPixels.type === "NEW_DRAW") {
                setState({
                    mouseDown: false,
                    pixelsArray: decodedPixels.pixelsArray
                });
                const context = canvas.current.getContext('2d');
                const imageData = context.createImageData(1, 1);
                decodedPixels.pixelsArray.forEach(item => {
                    const d = imageData.data;
                    d[0] = 0;
                    d[1] = 0;
                    d[2] = 0;
                    d[3] = 255;

                    context.putImageData(imageData, item.x, item.y);
                })
            }
        };

        return () => ws.current.close();
    }, []);

    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};


export default App;
